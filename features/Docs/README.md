# DESCRIPTION
1. The project consists in the automatic testing of a denied payment of a reservation through Ryanair.
2. Implemented in Python, Selenium, Behave.
3. Execution mode 
     - (CMD):C:\{PATH_PROJECT}\RyanairTest>C:\{PATH_PYTHON}\python.exe features\RunBDDTest.py
        (It is not tested in MAC OX because I do not have the environment)
     - Changes: 
        - In file RunBDDTest.py: To execute the test in Debug mode in some IDE, uncomment the following line of the file: features_file_path = ' Booking_up_payment.feature ' and comment on the following line: features_file_path = ' features/Booking_up_payment.feature '
        - In step_impl_booking_up_payment.py : To execute the test in Debug mode in some IDE, uncomment the following line of the file: chromeDriverExe = 'utilities\\chromedriver.exe' and comment on the following line: chromeDriverExe = 'C:\\{PATH_PROJECT}\\RyanairTest\\features\\utilities\\chromedriver.exe'
        - In step_impl_booking_up_payment.py : To run from CMD the test, put the absolute path of the ChromeDriver in the variable "chromeDriverExe" 

# Folders & files:
## Folders
- Docs/ : README.md and Documentation.txt files
- pages/ :
    - Page_Object_BookingUp.py
    - Page_Object_Generic.py
    - Page_Object_Login.py
    - Page_Object_Payment.py

- steps/ :
    - step_def_booking_up_payment.py : Defined steps definitions
    - step_impl_booking_up_payment.py : Implements steps definitions

- utilities/ : chromedriver exe
- reports_folder/ : Folder with the results of the test   

## Files
- Booking_up_payment.feature : User history of the automatic test
- RunBDDTest.py : Project Main

## Installations
- Python, Behave
