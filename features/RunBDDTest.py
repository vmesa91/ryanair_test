#!/usr/bin/python
# -*- coding: utf-8 -*-


import sys
import os
from shutil import rmtree
from behave import __main__ as runner_with_options


if __name__ == '__main__':
    sys.stdout.flush()
    reporting_folder_name = 'reports_folder'

    ## Remove if any reporting folder exists
    if os.path.exists(reporting_folder_name):
        rmtree(reporting_folder_name)
    os.mkdir(reporting_folder_name)

    ## Allure : Command line arguments
    reporting_allure = '-f allure_behave.formatter:AllureFormatter -o ' + reporting_folder_name + ' '

    ## Features File Path
    # Run
    features_file_path = ' features/Booking_up_payment.feature '
    # Debug
    #features_file_path = ' Booking_up_payment.feature '

    ## Tag Options
    tags_options = ' --tags=booking_up_wrong'

    ## Command line argument to capture console output
    #common_runner_options = ' --no-capture --no-capture-stderr -f plain '

    ## Full list of command line options
    fullRunnerOptions = tags_options + features_file_path + reporting_allure

    ## Run Behave + BDD + Steps
    runner_with_options.main(fullRunnerOptions)


