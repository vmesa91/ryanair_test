#!/usr/bin/python
# -*- coding: utf-8 -*-

# Imports
from selenium.common.exceptions import TimeoutException
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions
from page_objects import PageObject


delay_min = 3 # sec
delay_medium = 5 # sec
delay_max = 9 # sec

class General_Actions(PageObject):

    def wait_for_following_page_title(self, browser, expected_title):
        try:
            WebDriverWait(browser, delay_min).until(expected_conditions.title_contains(expected_title))
            return True
        except TimeoutException:
            raise Exception('Page with required title not found :' + expected_title + 'actual title is: ' + browser.title)


    def webdriver_shutdown(self, browser):
        browser.close()
        browser.quit()