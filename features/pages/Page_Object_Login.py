#!/usr/bin/python
# -*- coding: utf-8 -*-


# Imports
from page_objects import PageObject
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions
from selenium.webdriver.common.by import By
from selenium.webdriver.common.keys import Keys


delay_min = 3 # sec
delay_medium = 5 # sec
delay_max = 9 # sec

class Login(PageObject):

    # Locators : login_window
    login = (By.CSS_SELECTOR, '#myryanair-auth-login > a')
    link_forgot = (By.LINK_TEXT, 'Forgot password?')

    # Locators : login_message_success
    val_login = (By.CLASS_NAME, 'avatar-user')

    def login_window(self,user,password,browser):

        browser.find_element(*self.login).click()
        WebDriverWait(browser, delay_max).until(expected_conditions.element_to_be_clickable(self.link_forgot))
        browser.find_elements_by_xpath("//input[@placeholder='Email address']")[1].send_keys(user)
        browser.find_elements_by_xpath("//input[@placeholder='Password']")[1].send_keys(password)
        browser.find_elements_by_xpath("//input[@placeholder='Password']")[1].send_keys(Keys.ENTER)

    def login_message_success(self,browser):

        WebDriverWait(browser, delay_max).until(expected_conditions.presence_of_element_located(self.val_login))
