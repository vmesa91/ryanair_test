#!/usr/bin/python
# -*- coding: utf-8 -*-


# Imports
from page_objects import PageObject
from selenium.webdriver.common.by import By
from selenium.webdriver.support import expected_conditions
from random import choice
from selenium.webdriver.support.ui import WebDriverWait
import time


delay_min = 2 # sec
delay_medium = 5 # sec
delay_max = 9 # sec


class Payment(PageObject):

    # Locators : fill_passenger_data
    locator_details = (By.CLASS_NAME,'passengers-form')
    first_name = (By.XPATH, '//div[@class="form-field payment-passenger-first-name"]/input')
    last_name = (By.XPATH, '//div[@class="form-field payment-passenger-last-name"]/input')
    title = (By.XPATH, '//div[@class="form-field payment-passenger-title"]/div/select')
    country_number = (By.NAME, 'phoneNumberCountry')
    number = (By.XPATH, '//div[@class="phone-number"]/input')
    address1 = (By.ID, 'billingAddressAddressLine1')
    address2 = (By.ID, 'billingAddressAddressLine2')
    city = (By.ID, 'billingAddressCity')
    pd = (By.ID, 'billingAddressPostcode')
    country = (By.ID, 'billingAddressCountry')

    # Locators : fill_card_data_and_pay
    card_number = (By.NAME, 'cardNumber')
    card_type = (By.NAME, 'cardType')
    expiry_month = (By.NAME, 'expiryMonth')
    expiry_year = (By.NAME, 'expiryYear')
    security_code = (By.NAME, 'securityCode')
    card_holder_name = (By.NAME, 'cardHolderName')
    accept_policy = (By.CLASS_NAME, 'core-checkbox-label')
    button_pay_now = (By.CSS_SELECTOR, 'button.core-btn-primary[ng-click^="$ctrl.processPayment()"]')

    # Vars
    name = ['SODMESDE', 'SDSRECSS', 'ROEFMODSMO', 'WQDMSIASAD', 'SAWEDWDAD']
    name2 = ['SMDKSDMAK', 'DASDASDA', 'ASDADASDAW', 'DASDASDADAS' , 'DSDEWDASD']
    text_error = 'As your payment was not authorised we could not complete your reservation. Please ensure that the information was correct or use a new payment to try again'

    def fill_passenger_data(self,browser):

        # Forms Data
        time.sleep(delay_medium)
        WebDriverWait(browser, delay_max).until(expected_conditions.presence_of_element_located(self.locator_details))
        time.sleep(delay_min)
        for input_first_name in browser.find_elements(*self.first_name):
            input_first_name.send_keys(choice(self.name))

        for input_last_name in browser.find_elements(*self.last_name):
            input_last_name.send_keys(choice(self.name2))

        for select_title in browser.find_elements(*self.title):
            select_title.click()
            select_title.send_keys('Mr')

        browser.find_element(*self.number).send_keys('654987321')
        browser.find_element(*self.country_number).send_keys('Spain')
        browser.find_element(*self.address1).send_keys('Address1')
        browser.find_element(*self.address2).send_keys('Address2')
        browser.find_element(*self.city).send_keys('Madrid')
        browser.find_element(*self.pd).send_keys('25087')
        browser.find_element(*self.country).send_keys('Spain')

    def fill_card_data_and_pay(self,number_card,date_card,CVC_card,browser):

        browser.find_element(*self.card_number).send_keys(number_card)
        browser.find_element(*self.card_type).send_keys('MasterCard')
        browser.find_element(*self.expiry_month).send_keys(date_card.split('/')[0])
        browser.find_element(*self.expiry_year).send_keys('20' + date_card.split('/')[1])
        browser.find_element(*self.security_code).send_keys(CVC_card)
        browser.find_element(*self.card_holder_name).send_keys(choice(self.name2))

        # Accept Policy
        time.sleep(delay_medium)
        browser.find_elements(*self.accept_policy)[8].click()
        # Pay
        time.sleep(delay_medium)
        browser.find_element(*self.button_pay_now).click()

    def verify_error_payment(self,browser):
        if (self.text_error in browser.page_source):
            return True


