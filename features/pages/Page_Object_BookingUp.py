#!/usr/bin/python
# -*- coding: utf-8 -*-


# Imports
from page_objects import PageObject
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions
from selenium.webdriver.common.by import By
from selenium.webdriver.common.keys import Keys
from datetime import datetime
import time


delay_min = 2 # sec
delay_medium = 5 # sec
delay_max = 9 # sec


class BookingUp(PageObject):

    # Locators : fill_flight_data
    destination_from = (By.XPATH, "//input[@placeholder='Departure airport']")
    destination_to = (By.XPATH, "//input[@placeholder='Destination airport']")
    passengers = (By.NAME, 'passengers')
    passenger_adults = (By.XPATH, "//div[@value='paxInput.adults']/div/div/input")
    passenger_children = (By.XPATH, "//div[@value='paxInput.children']/div/div/input")
    passenger_infants = (By.XPATH, "//div[@value='paxInput.infants']/div/div/input")
    search_flights = (By.CSS_SELECTOR, 'button.core-btn-primary[ng-click^="searchFlights()"]')
    flight_one_way = (By.CSS_SELECTOR, '#flight-search-type-option-one-way')
    pop_up_confirm_travel_in_day = (By.CLASS_NAME, 'dialog-body')
    pop_up_cabin_bags = (By.CSS_SELECTOR, 'div.popup-msg__close[ng-click^="closeThisDialog()"]')

    # Locators : flights_available
    flight_out = (By.ID, 'outbound')

    # Locators : choose_flight_and_accept_mandate
    continue_pay = (By.CSS_SELECTOR, 'button.core-btn-primary[ng-click^="$ctrl.onContinueBtnClick()"]')

    # Locators : choose_seats_and_accept_them
    confirm_seats = (By.CSS_SELECTOR, 'button.core-btn-primary[ng-click^="confirm()"]')
    confirm_seats_OK = (By.CSS_SELECTOR, 'button.core-btn-primary[ng-click^="!$ctrl.isLoading && $ctrl.onClick()"]')
    priority_exit = (By.CSS_SELECTOR, 'div.priority-boarding-with-bags-popup__close-x[ng-click^="closeThisDialog()"]')
    check_out = ((By.CSS_SELECTOR, 'button.core-btn-primary[ng-click^="$ctrl.onContinueBtnClick()"]'))
    choose_seats = ((By.CSS_SELECTOR,'span.seat-click[ng-click^="$ctrl.addSeat(seat)"]'))

    #Vars
    close_this_dialog = "closeThisDialog('confirm')"
    datein_booking_up = ""

    def fill_flight_data(self,context,data_booking_up,item_details,browser):

        browser.find_element(*self.flight_one_way).click()
        time.sleep(1)
        for row in context.table:
            context.temp_item_data = row['data_booking_up']
            context.temp_item_detail = row['item_details']

            if (row['data_booking_up'] == 'country_from'):
                browser.find_element(*self.destination_from).clear()
                browser.find_element(*self.destination_from).send_keys(row['item_details'])
                browser.find_element(*self.destination_from).send_keys(Keys.ENTER)
                time.sleep(1)

            if (row['data_booking_up'] == 'country_to'):
                browser.find_element(*self.destination_to).clear()
                browser.find_element(*self.destination_to).send_keys(row['item_details'])
                browser.find_element(*self.destination_to).send_keys(Keys.ENTER)
                time.sleep(1)

            if (row['data_booking_up'] == 'date_in'):
                self.datein_booking_up = row['item_details']
                browser.find_elements_by_name('dateInput0')[0].clear()
                browser.find_elements_by_name('dateInput0')[0].send_keys(row['item_details'].split('/')[0])
                browser.find_elements_by_name('dateInput1')[0].clear()
                browser.find_elements_by_name('dateInput1')[0].send_keys(row['item_details'].split('/')[1])
                browser.find_elements_by_name('dateInput2')[0].clear()
                browser.find_elements_by_name('dateInput2')[0].send_keys(row['item_details'].split('/')[2])
                time.sleep(1)

            if (row['data_booking_up'] == 'num_adults'):
                browser.find_element(*self.passengers).click()
                # self.browser.find_element_by_css_selector('button.core-btn[ng-click^="$ctrl.increment()"]').click()
                browser.find_element(*self.passenger_adults).clear()
                browser.find_element(*self.passenger_adults).send_keys(row['item_details'])
                time.sleep(1)

            if (row['data_booking_up'] == 'num_childs'):
                browser.find_element(*self.passenger_children).clear()
                browser.find_element(*self.passenger_children).send_keys(row['item_details'])
                time.sleep(1)

            if (row['data_booking_up'] == 'num_infants'):
                browser.find_element(*self.passenger_infants).clear()
                browser.find_element(*self.passenger_infants).send_keys(row['item_details'])
                time.sleep(1)

                browser.find_element(*self.search_flights).click()

        time.sleep(delay_min)
        if (datetime.today().strftime("%d/%m/%Y") >= self.datein_booking_up):
            WebDriverWait(browser, delay_max).until(expected_conditions.presence_of_element_located(self.pop_up_confirm_travel_in_day))
            browser.find_element_by_css_selector('button.core-btn-primary[ng-click^="' + self.close_this_dialog + '"]').click()
        else:
            pass

    def flights_available(self,browser):
        if ("Sold out" in browser.find_element(*self.flight_out).text):
            raise Exception('Sorry, there are no flights available on this day')
        else:
            return True

    def choose_flight_and_accept_mandate(self,browser):
        for btn in browser.find_elements_by_class_name('core-btn-primary'):
            if ('€' in btn.text):
                btn.click()
                time.sleep(delay_min)
                break

        browser.find_elements_by_class_name('flights-table-fares__benefit-list')[0].click()
        time.sleep(delay_min)
        browser.find_element(*self.continue_pay).click()

    def choose_seats_and_accept_them(self,browser):

        browser.find_element(*self.confirm_seats).click()
        time.sleep(delay_min)
        seats = browser.find_elements(*self.choose_seats)

        for seat in seats:
            try:
                time.sleep(1)
                seat.click()
                if browser.find_elements_by_tag_name("button")[7].is_enabled():
                    break
            except:
                pass
        browser.find_elements_by_tag_name("button")[7].click()
        time.sleep(delay_min)
        browser.find_element(*self.confirm_seats_OK).click()
        time.sleep(delay_min)
        browser.find_element(*self.priority_exit).click()
        try:
            time.sleep(delay_medium)
            browser.find_element(*self.check_out).click()
        except:
            pass
        try:
            browser.find_element(*self.check_out).click()
            browser.find_element(*self.pop_up_cabin_bags).click()
            time.sleep(delay_min)
        except:
            pass