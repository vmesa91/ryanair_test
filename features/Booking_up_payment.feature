Feature: A booking up payment Ryanair

  @booking_up_success
  Scenario: Booking up payment success
    Given Go to Ryanair browser "https://www.ryanair.com/ie/en/"
    When  Log In with the correct user "v.mesagetafe@gmail.com" and the correct password "1234Rya."
    Then  Displays successful login message
    Given I make a booking with the following information "<data_booking_up>" and "<item_details>"
      | data_booking_up  | item_details   |
      | country_from     |  DUB           |
      | country_to       |  SXF           |
      | date_in          |  25/06/2018    |
      | num_adults       |  2             |
      | num_childs       |  1             |
      | num_infants      |  0             |
    When  I pay for booking with correct card details "5555555555555555", "10/18" and "265"
    Then  I should get payment accept message


  @booking_up_wrong
  Scenario: Booking up payment unsuccess
    Given Go to Ryanair browser "https://www.ryanair.com/ie/en/"
    When  Log In with the correct user "v.mesagetafe@gmail.com" and the correct password "1234Rya."
    Then  Displays successful login message
    Given I make a booking with the following information "<data_booking_up>" and "<item_details>"
      | data_booking_up  | item_details   |
      | country_from     |  DUB           |
      | country_to       |  SXF           |
      | date_in          |  25/06/2018    |
      | num_adults       |  2             |
      | num_childs       |  1             |
      | num_infants      |  0             |
    When  I pay for booking with incorrect card details "5555555555555557", "10/18" and "265"
    Then  I should get payment declined message


