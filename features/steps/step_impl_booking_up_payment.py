#!/usr/bin/python
# -*- coding: utf-8 -*-

# Imports
from selenium import webdriver
from features.pages.Page_Object_Generic import *
from features.pages.Page_Object_Login import *
from features.pages.Page_Object_BookingUp import *
from features.pages.Page_Object_Payment import *

import time

# Vars
# Run
chromeDriverExe = 'C:\\{PATH_PROJECT}\\RyanairTest\\features\\utilities\\chromedriver.exe'
#Debug
#chromeDriverExe = 'utilities\\chromedriver.exe'
delay_min = 2 # sec
delay_medium = 4 # sec
delay_max = 10 # sec

class HandleClassImpl:

    def __init__(self):
        pass

    ## Open browser
    def go_to_ryanair_browser(self,url,handler_chrome):
        print('Performing initial set up, invoking browser')
        handler_chrome.append(webdriver.Chrome(executable_path=chromeDriverExe))
        browser = handler_chrome[0]
        browser.maximize_window()
        browser.delete_all_cookies()
        browser.get(url)
        general_action = General_Actions(browser)
        general_action.wait_for_following_page_title(browser,'Official Ryanair website')
        return

    ## Log in at Ryanair: Required for booking
    def log_in_ryanair(self,user,password,handler_chrome):
        browser = handler_chrome[0]
        login = Login(browser)
        login.login_window(user,password,browser)
        return

    ## Verify that the login is correct
    def verification_displays_succeess_login(self, handler_chrome):
        browser = handler_chrome[0]
        login = Login(browser)
        login.login_message_success(browser)
        return

    ## Make the reservation with the given data.
    ## Choose seats.
    ## Validate pop-up.
    def booking_up_with_data(self,context,data_booking_up,item_details,handler_chrome):
        # Locators
        mandatory_seats = (By.CLASS_NAME, 'mandatory-seats-description')
        flight_out = (By.ID, 'outbound')

        browser = handler_chrome[0]
        booking_up = BookingUp(browser)
        booking_up.fill_flight_data(context, data_booking_up, item_details, browser)

        WebDriverWait(browser, delay_max).until(expected_conditions.url_changes)
        time.sleep(delay_medium)
        WebDriverWait(browser, delay_max).until(expected_conditions.presence_of_element_located(flight_out))

        if (booking_up.flights_available(browser)):
            booking_up.choose_flight_and_accept_mandate(browser)
            WebDriverWait(browser, delay_max).until(expected_conditions.presence_of_element_located(mandatory_seats))
            booking_up.choose_seats_and_accept_them(browser)
            return

    ## Fill passenger data.
    ## Fill in credit card information.
    ## Accept conditions of use.
    ## Proceed payment.
    def pay_booking_up_with_incorrect_info(self,number_card,date_card,CVC_card,handler_chrome):
       browser = handler_chrome[0]
       payment = Payment(browser)
       payment.fill_passenger_data(browser)
       payment.fill_card_data_and_pay(number_card,date_card,CVC_card,browser)
       return

    # Verify payment denied.
    def verification_displays_incorrect_message_payment(self,handler_chrome):
        browser = handler_chrome[0]
        payment = Payment(browser)
        payment.verify_error_payment(browser)
        general_action = General_Actions(browser)
        general_action.webdriver_shutdown(browser)
        return

obj_handle_impl = HandleClassImpl()