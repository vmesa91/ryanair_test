#!/usr/bin/python
# -*- coding: utf-8 -*-

from behave import *
from features.steps.step_impl_booking_up_payment import *

handler_chrome = []

@given('Go to Ryanair browser "{url}"')
def step_impl(context, url):
    obj_handle_impl.go_to_ryanair_browser(url, handler_chrome)

@when('Log In with the correct user "{user}" and the correct password "{password}"')
def step_impl(context,user,password):
    obj_handle_impl.log_in_ryanair(user, password, handler_chrome)

@then('Displays successful login message')
def step_impl(context):
    obj_handle_impl.verification_displays_succeess_login(handler_chrome)

@given('I make a booking with the following information "{data_booking_up}" and "{item_details}"')
def step_impl(context,data_booking_up,item_details):
    obj_handle_impl.booking_up_with_data(context, data_booking_up, item_details, handler_chrome)

@when('I pay for booking with correct card details "{number_card}", "{date_card}" and "{CVC_card}"')
def step_impl(context,number_card,date_card,CVC_card):
    pass

@then('I should get payment accept message')
def step_impl(context):
    pass

@when('I pay for booking with incorrect card details "{number_card}", "{date_card}" and "{CVC_card}"')
def step_impl(context,number_card,date_card,CVC_card):
    obj_handle_impl.pay_booking_up_with_incorrect_info(number_card, date_card, CVC_card, handler_chrome)

@then('I should get payment declined message')
def step_impl(context):
    obj_handle_impl.verification_displays_incorrect_message_payment(handler_chrome)



